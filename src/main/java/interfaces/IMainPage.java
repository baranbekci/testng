package interfaces;

public interface IMainPage {

    void clickSignIn(String text);

    void enterEmail(String email);

    void enterFirstName(String name);

    void enterLastName(String lastName);

    void enterPassword(String password);

    void enterAddress(String arg1);

    void enterCity(String arg1);

    void selectState(String arg1);

    void enterZipCode(String arg1);

    void enterMobilePhone(String mobilePhone);

    void clickRegisterButton();
}
