package pages;

import constant.MainPageXpathConstants;
import interfaces.IMainPage;
import org.openqa.selenium.WebDriver;
import sun.jvm.hotspot.debugger.Page;

public class MainPage extends PageObject implements IMainPage {
    public MainPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void clickSignIn(String text){
        super.clickElementByXpath(String.format(MainPageXpathConstants.SIGN_IN, text));
    }

    @Override
    public void enterEmail(String email){
        super.enterTextByXpath(MainPageXpathConstants.EMAIL_INPUT_AREA, email);
    }

    @Override
    public void enterFirstName(String name){
        super.enterTextByXpath(MainPageXpathConstants.ENTER_FIRST_NAME, name);
    }

    @Override
    public void enterLastName(String lastName){
        super.enterTextByXpath(MainPageXpathConstants.ENTER_LAST_NAME, lastName);
    }

    @Override
    public void enterPassword(String password){
        super.enterTextByXpath(MainPageXpathConstants.ENTER_PASSWORD, password);
    }

    @Override
    public void enterAddress(String arg1){
        super.enterTextByXpath(MainPageXpathConstants.ENTER_ADDRESS, arg1);
    }

    @Override
    public void enterCity(String arg1){
        super.enterTextByXpath(MainPageXpathConstants.ENTER_CITY, arg1);
    }

    @Override
    public void selectState(String arg1){
        super.enterTextByXpath(MainPageXpathConstants.STATE_SELECT_BOX, arg1);
    }

    @Override
    public void enterZipCode(String arg1){
        super.enterTextByXpath(MainPageXpathConstants.ENTER_ZIP_CODE, arg1);
    }

    @Override
    public void enterMobilePhone(String mobilePhone){
        super.enterTextByXpath(MainPageXpathConstants.ENTER_MOBILE_PHONE, mobilePhone);
    }

    @Override
    public void clickRegisterButton(){
        super.clickElementByXpath(MainPageXpathConstants.REGUSTER_BUTTON);
    }
}
