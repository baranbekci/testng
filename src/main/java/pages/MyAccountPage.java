package pages;

import constant.MyAccountPageXpathConstants;
import interfaces.IMyAccountPage;
import org.openqa.selenium.WebDriver;

public class MyAccountPage extends MainPage implements IMyAccountPage {
    public MyAccountPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void clickTshirtCategory(){
        super.waitByXpath(MyAccountPageXpathConstants.CATEGORY_THSIRTS);
        super.clickElementByXpath(MyAccountPageXpathConstants.FADED_SHORT_SLEEVE_TSHIRTS);
    }

    @Override
    public void clickAddToCardFirstButton(){
        super.waitByXpath(MyAccountPageXpathConstants.FADED_SHORT_SLEEVE_TSHIRTS);
        super.clickLastElement(MyAccountPageXpathConstants.FADED_SHORT_SLEEVE_TSHIRTS);
    }
}
