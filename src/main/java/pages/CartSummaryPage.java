package pages;

import constant.CartSummaryXpathConstants;
import interfaces.ICartSummaryPage;
import org.openqa.selenium.WebDriver;

public class CartSummaryPage extends PageObject implements ICartSummaryPage {
    public CartSummaryPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean chekcPrice(){
        String price = super.getTextOfElement(CartSummaryXpathConstants.PRICE);
        String totalPrice = super.getTextOfElement(CartSummaryXpathConstants.TOTAL_PRICE);
        return price.contains("$16.51") && totalPrice.contains("$18.51");
    }
}
