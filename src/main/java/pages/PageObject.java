package pages;

import constant.TestSettingConstants;
import interfaces.IPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

public class PageObject implements IPageObject {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected Action action;

    public PageObject(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, TestSettingConstants.WAIT_TIMEOUT_IN_SECONDS);
    }

    @Override
    public void clickElementByXpath(String xpath){
        try {
            By waitBy = waitByXpath(xpath);
            this.driver.findElement(waitBy).click();
        } catch (Exception e) {
            Assert.fail(String.format("%s xpath element cannot be clicked, %s", xpath, e.getMessage()));
        }
    }

    @Override
    public void selectByText(String selectXpath, String option){
        try{
            Select select = new Select(this.driver.findElement(By.xpath(selectXpath)));
            select.selectByVisibleText(option);
        }catch (Exception e){
            Assert.fail(String.format("%s text was not selected from, %s", option, e.getMessage()));
        }
    }

    @Override
    public By waitByXpath(String xpath){
        By waitBy = null;
        try{
            waitBy = By.xpath(xpath);
            this.wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(waitBy, 0));
        } catch (Exception e){
            Assert.fail(String.format("%s xpath not found, %s", xpath, e.getMessage()));
        }
        return waitBy;
    }

    @Override
    public void enterTextByXpath(String xpath, String text){
        try{
            By waitBy = waitByXpath(xpath);
            WebElement webElement = this.driver.findElement(waitBy);
            webElement.clear();
            webElement.click();
            webElement.sendKeys(text);
        } catch (Exception e){
            Assert.fail(String.format("%s text cannot be entered into, %s", text, xpath));
        }
    }

    @Override
    public void hover(String xpath){
        try{
            By waitBy = waitByXpath(xpath);
            Actions actions = new Actions(driver);
            WebElement categoryOption = this.driver.findElement(waitBy);
            actions.moveToElement(categoryOption).perform();
        } catch (Exception e){
            Assert.fail(String.format("%s element cannot be hovered, %s", xpath, e.getMessage()));
        }
    }

    @Override
    public void clickLastElement(String xpath){
        try{
            By waitBy = waitByXpath(xpath);
            WebElement existingElement = null;
            for(WebElement elements: driver.findElements(waitBy)){
                existingElement = elements;
            }
            assert existingElement != null;
            existingElement.click();
        }catch (Exception e){
            Assert.fail(xpath + " element cannot be clicked " + e.getMessage());
        }
    }

    @Override
    public void clickFirstElement(String xpath) {
        try{
            By waitBy = waitByXpath(xpath);
            driver.findElements(waitBy).stream().findFirst().ifPresent(WebElement::click);
        } catch (Exception e){
            Assert.fail(xpath + " element cannot be clicked " + e.getMessage());
        }
    }

    @Override
    public String getTextOfElement(String xpath){
        String text = "";
        try{
            By waitBy = waitByXpath(xpath);
            text = this.driver.findElement(waitBy).getText();
        }catch (Exception e){
            Assert.fail(String.format("%s element cannot be clicked, %s ", xpath, e.getMessage()));
        }
        return text;
    }

    @Override
    public Boolean isElementExist(String xpath){
        try{
            this.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            if(this.driver.findElements(By.xpath(xpath)).size() > 0){
                return true;
            }
        } catch (Exception e){
            Assert.fail(String.format("%s xpath element cannot be clicked, %s ", xpath, e.getMessage()));
        }
        return false;
    }

    @Override
    public String getTextOfElementFirstElement(String xpath){
        try{
            By waitBy = waitByXpath(xpath);
            WebElement existingElements = null;
            for(WebElement elements: driver.findElements(waitBy)){
                existingElements = elements;
                break;
            }
            assert existingElements != null;
            return existingElements.getAttribute("textContent");
        } catch (Exception e){
            Assert.fail(" element cannot be clicked, " + e.getMessage());
        }
        return null;
    }

    @Override
    public String getValueOfElement(String xpath){
        String value = "";
        try{
            By waitBy = waitByXpath(xpath);
            value = this.driver.findElement(waitBy).getAttribute("value");
            return value;
        } catch (Exception e){
            Assert.fail(String.format("%s xpath element cannot be clicked, %s ", xpath, e.getMessage()));
        }
        return value;
    }

    @Override
    public void pressEnter(){
        Robot robot = null;
        try{
            robot = new Robot();
        } catch (AWTException e){
            e.printStackTrace();
        }
        assert robot != null;
        robot.setAutoWaitForIdle(true);
        robot.setAutoDelay(2000);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }
}
