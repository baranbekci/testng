package constant;

public class MainPageXpathConstants {
    public static final String SIGN_IN = "//*[contains(@title, '%s')]";
    public static final String EMAIL_INPUT_AREA = "//*[@id='email_create']";
    public static final String CREATE_ACCOUNT_BUTTON = "//*[@id='SubmitCreate']";
    public static final String SIGH_IN_PAGE_TITLE = "//*[text()='Your personal information']";
    public static final String ENTER_FIRST_NAME = "//*[@id='customer_firstname']";
    public static final String ENTER_LAST_NAME = "//*[@id='customer_lastname']";
    public static final String ENTER_PASSWORD = "//*[@id='passwd']";
    public static final String ENTER_ADDRESS = "//*[@id='address1']";
    public static final String ENTER_CITY = "//*[@id='city']";
    public static final String STATE_SELECT_BOX = "//*[@id='id_state']";
    public static final String ENTER_ZIP_CODE = "//*[@id='postcode']";
    public static final String ENTER_MOBILE_PHONE = "//*[@id='phone_mobile']";
    public static final String REGUSTER_BUTTON = "//*[text()='Register']";
    public static final String MY_ACCOUNT_TITLE = "//h1[text()='My account']";

}
