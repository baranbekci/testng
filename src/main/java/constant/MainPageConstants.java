package constant;

public class MainPageConstants {
    public static final String FIRST_NAME = "firstnameInput";
    public static final String LAST_NAME = "lastnameInput";
    public static final String PASSWORD = "P@ssword+'^^23";
    public static final String ADDRESS = "addressInput";
    public static final String CITY = "cityInput";
    public static final String STATE = "Alaska";
    public static final String ZIP_CODE = "00001";
    public static final String MOBILE_PHONE = "0123123123";
}
