package constant;

public class CartSummaryXpathConstants {
    public static final String TITLE = "//h1[@id='cart_title']";
    public static final String PRICE = "//td[@id='total_product']";
    public static final String TOTAL_PRICE = "//span[@id='total_price']";
}
