import constant.MainPageXpathConstants;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Date;

public class TC001 extends BaseTest{
    Date date = new Date();

    @BeforeClass
    public void beforeClass(){
        mainPage.clickSignIn("Log in to your");
        mainPage.enterEmail("test@auto" + date.getTime() + ".com");
    }

    @Test
    public void Case3_TC001(){
        mainPage.enterFirstName(MainPageXpathConstants.ENTER_FIRST_NAME);
        mainPage.enterLastName(MainPageXpathConstants.ENTER_LAST_NAME);
        mainPage.enterPassword(MainPageXpathConstants.ENTER_PASSWORD);
        mainPage.enterAddress(MainPageXpathConstants.ENTER_ADDRESS);
        mainPage.enterCity(MainPageXpathConstants.ENTER_CITY);
        mainPage.selectState(MainPageXpathConstants.STATE_SELECT_BOX);
        mainPage.enterZipCode(MainPageXpathConstants.ENTER_ZIP_CODE);
        mainPage.enterMobilePhone(MainPageXpathConstants.ENTER_MOBILE_PHONE);
        mainPage.clickRegisterButton();

        cartSummaryPage.chekcPrice();

        myAccountPage.clickTshirtCategory();
        myAccountPage.clickAddToCardFirstButton();

    }
}
