import constant.TestSettingConstants;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import pages.CartSummaryPage;
import pages.MainPage;
import pages.MyAccountPage;
import util.WebDriverFactory;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    protected static WebDriver driver;
    protected static MainPage mainPage;
    protected static MyAccountPage myAccountPage;
    protected static CartSummaryPage cartSummaryPage;

    @BeforeSuite
    public static void setUpSuite(){
        WebDriverFactory webDriverFactory = new WebDriverFactory();
        String url = TestSettingConstants.URL;
        driver = webDriverFactory.createWebDriver(TestSettingConstants.CHROME);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.get(url);
        driver.manage().window().maximize();

        mainPage = new MainPage(driver);
        myAccountPage = new MyAccountPage(driver);
        cartSummaryPage = new CartSummaryPage(driver);
    }

    @AfterSuite
    public static void tearDownSuite(){
        driver.close();
    }
}
